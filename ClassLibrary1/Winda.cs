﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Glowny.Kontrakt;
using Wyswietlacz.Kontrakt;

namespace Glowny.Implementacja
{
    public class WindaKosmiczna : IWindaKosmiczna //TODO IWYSWIETLACZ
    {
        private IWyswietlacz wyswietl;

        public void Wjedz()
        {
            Console.WriteLine("wjazd");
        }

        public void Otworzsie()
        {
            Console.WriteLine("Otworz sie");
        }

        public WindaKosmiczna(IWyswietlacz wyswietlacz)
        {
            this.wyswietl = wyswietlacz;
        }
    }
}
