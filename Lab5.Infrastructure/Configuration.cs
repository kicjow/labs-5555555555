﻿using System;
using PK.Container;
using MojKontener;
using Wyswietlacz.Kontrakt;
using Wyswietlacz.Implementacja;
using Glowny.Implementacja;
using Glowny.Kontrakt;
namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IWyswietlacz wyswietlacz = new Wyswietlacz.Implementacja.Wyswietlacz();
            IWindaKosmiczna winda = new WindaKosmiczna(wyswietlacz);
            MojContainer kontener = new MojContainer();
            kontener.Register<IWyswietlacz>(wyswietlacz);
            kontener.Register<IWindaKosmiczna>(winda);

            return kontener;
        }
    }
}
